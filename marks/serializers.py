from marks.models import Student, Marks

from rest_framework import serializers


class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = "__all__"


class MarksSerializer(serializers.ModelSerializer):
    student_name = serializers.ReadOnlyField(source='student.name')

    class Meta:
        model = Marks
        fields = ('id',
                  'student',
                  'student_name',
                  'english_marks',
                  'science_marks',
                  'maths_marks',)

    # def to_representation(self, instance):
    #     rep = super(StudentSerializer, self).to_representation(instance)
    #     rep['student'] = instance.student.name
    #     return rep
