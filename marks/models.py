from django.db import models

# Create your models here.


class Student(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    roll_no = models.IntegerField(blank=True, null=True)
    standard = models.CharField(max_length=40, choices=(
        ('10th', '10th'), ('11th', '11th'), ('12th', '12th')), blank=True, null=True)

    def __str__(self):
        return self.name


class Marks(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, blank=True, null=True)
    english_marks = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True)
    science_marks = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True)
    maths_marks = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True)

    def __str__(self):
        return self.student.name
