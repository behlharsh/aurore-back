from django.shortcuts import render
from marks.models import Student, Marks
from marks.serializers import MarksSerializer, StudentSerializer
# Create your views here.
from rest_framework import viewsets, permissions


class StudentViewSet(viewsets.ModelViewSet):
    """ViewSet for the Student class"""

    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    # permission_classes = [permissions.IsAuthenticated]


class MarksViewSet(viewsets.ModelViewSet):
    """ViewSet for the Marks class"""
    queryset = Marks.objects.all()
    serializer_class = MarksSerializer
    # permission_classes = [permissions.IsAuthenticated]
